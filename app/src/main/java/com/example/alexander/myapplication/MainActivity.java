package com.example.alexander.myapplication;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
class States {
    public States() {
        states = new HashMap();
    }

    private Map states;

    public void Load() {
        states.clear();

        {
            State state = new State(
                    0,
                    "Start - 0",
                    new Transition(1, "To 1"),
                    new Transition(2, "To 2"),
                    new Transition(1, "To 1"),
                    new Transition(0, "To 0")
            );
            states.put(state.id, state);
        }
        {
            State state = new State(
                    1,
                    "State - 1",
                    new Transition(0, "To 0"),
                    new Transition(1, "To 1"),
                    new Transition(1, "To 1"),
                    new Transition(0, "To 0")
            );
            states.put(state.id, state);
        }

        {
            State state = new State(
                    2,
                    "State - 2",
                    new Transition(0, "To 0"),
                    new Transition(0, "To 0"),
                    new Transition(1, "To 1"),
                    new Transition(2, "To 2")
            );
            states.put(state.id, state);
        }
    }

    public State getState(int state) {
        return (State) states.get(state);
    }
}
*/

/*
class Transition {
    public Transition(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int id;
    public String text;
}

class State {
    public State(int id, String text, Transition t1, Transition t2, Transition t3, Transition t4) {
        this.id = id;
        this.text = text;
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
    }

    int id;
    String text;
    Transition t1;
    Transition t2;
    Transition t3;
    Transition t4;
}

class StateN {
    public StateN(int id, String text) {
        this.id = id;
        this.text = text;
    }

    int id;
    String text;
    List<TransitionN> transitions;
}
*/


interface Transition {
    void update(Context context);
    String getName();
}

class Transition_Init implements Transition {
    public void update(Context context) {
        context.text = "";
        context.result = "Игра началась";

        context.health = 100;
        context.fatigue = 0;
        context.location = Location.Home;
        context.isDead = false;

        context.transitionsCurrentState.add(new Transition_goToHome()); // XXX
    }
    public String getName() { return ""; }
}

class Transition_1 implements Transition {
    public void update(Context context) {
        context.result = "Здоровье снизилось";
        context.health -= 10;
    }
    public String getName() { return "Уменьшить здоровье на 10"; }
}

class Transition_2 implements Transition {
    public void update(Context context) {
        context.result = "Здоровье увеличено";
        context.health += 10;
    }
    public String getName() { return "Увеличить здоровье на 10"; }
}

class Transition_3 implements Transition {
    public void update(Context context) {
        context.result = "Здоровье: " + context.health;
    }
    public String getName() { return "Показать текущее здоровье"; }
}

class Transition_4 implements Transition {
    public void update(Context context) {
        if (context.fatigue < 50) {
            context.result = "Чувствуете прилив сил";
            context.fatigue += 10;
            context.health += 10;
        } else {
            context.result = "Потом, устал";
        }
    }
    public String getName() { return "Сделать зарядку"; }
}

class Transition_debug implements Transition {
    public void update(Context context) {
        context.result = "health: " + context.health + '\n';
        context.result += "fatigue: " + context.fatigue + '\n';
        context.result += "isDead: " + context.isDead + '\n';
        context.result += "location: " + context.location.toString() + '\n';

        context.result += "transitions.size(): " + context.transitions.size() + '\n';
        context.result += "transitionsCurrentState.size(): " + context.transitionsCurrentState.size() + '\n';
        context.result += "transitionsPost.size(): " + context.transitionsPost.size() + '\n';
    }
    public String getName() { return "debug"; }
}

class Transition_goToSquare implements Transition {
    public void update(Context context) {
        context.result = "Красивая площадь";
        context.location = Location.Square;

        context.transitionsCurrentState.add(new Transition_goToMarket());
        context.transitionsCurrentState.add(new Transition_goToGate());
        context.transitionsCurrentState.add(new Transition_goToForest());
        context.transitionsCurrentState.add(new Transition_goToHome());
    }
    public String getName() { return "Идти на площадь"; }
}

class Transition_goToMarket implements Transition {
    public void update(Context context) {
        context.result = "Красивая площадь";
        context.location = Location.Market;

        context.transitionsCurrentState.add(new Transition_goToSquare());
    }
    public String getName() { return "Итди на рынок"; }
}

class Transition_goToGate implements Transition {
    public void update(Context context) {
        context.result = "Ворота полуоткрыты";
        context.location = Location.Gate;

        context.transitionsCurrentState.add(new Transition_goToSquare());
        context.transitionsCurrentState.add(new Transition_goToForest());
    }
    public String getName() { return "Идти к воротам"; }
}

class Transition_goToHome implements Transition {
    public void update(Context context) {
        context.result = "Старый дом";
        context.location = Location.Home;

        context.transitionsCurrentState.add(new Transition_goToSquare());
    }
    public String getName() { return "Идти домой"; }
}

class Transition_goToForest implements Transition {
    public void update(Context context) {
        context.result = "Не лучший выбор гулять по лесу в такое время";
        context.location = Location.Forest;

        context.transitionsCurrentState.add(new Transition_goToSquare());
        context.transitionsCurrentState.add(new Transition_goToGate());
    }
    public String getName() { return "Идти в лес"; }
}

class Transition_isDead implements Transition {
    public void update(Context context) {
        if (context.health < 0 || context.fatigue > 100) {
            context.result = "Смерть подкралась незаметно";
            context.isDead = true;
        }
    }
    public String getName() { return ""; }
}

enum Location { Market, Square, Home, Forest, Gate }

class Context {
    Context() {
        transitions = new ArrayList<>();
        transitions.add(new Transition_1());
        transitions.add(new Transition_2());
        transitions.add(new Transition_3());
        transitions.add(new Transition_4());
        transitions.add(new Transition_debug());

        transitionsCurrentState = new ArrayList<>();

        transitionsPost = new ArrayList<>();
        transitionsPost.add(new Transition_isDead());

        new Transition_Init().update(this);
    }

    public void update(int i) {

        if (i < 0)
            return;

        if (isDead)
            return;

        result = "";


        if (i < transitionsCurrentState.size()) {
            Transition transition = transitionsCurrentState.get(i);
            transitionsCurrentState.clear();
            transition.update(this);
        } else {
            i -= transitionsCurrentState.size();
            if (i < transitions.size()) {
                transitions.get(i).update(this);
            } else {
                result = "Unknown transition 2. " + i + " " + transitions.size();
            }
        }

        updatePost();
    }

    public List<String> getTransitionNames() {
        List<String> res = new ArrayList<>();
        for (Transition transition : transitionsCurrentState) {
            res.add(transition.getName());
        }
        for (Transition transition : transitions) {
            res.add(transition.getName());
        }
        return res;
    }
    
    private void updatePost() {
        for (Transition transition : transitionsPost) {
            transition.update(this);
        }
    }

    List<Transition> transitions;  // добавлять при инициализации и не менять.
    List<Transition> transitionsCurrentState; // сбрасывать после каждого update и добавлять заново новые.
    List<Transition> transitionsPost;  // вызывать update на каждом ходу.

    String result;
    String text;

    int health;
    int fatigue;
    boolean isDead;
    Location location;

    final int offset = 1000;
}

/*class Game {
    Game() {
        context = new Context();
    }

    public void update(int i) {
        context.update(i);
    }

    public void updateCurrentState(int i) {
        context.updateCurrentState(i);
    }

    private Context context;
}*/

class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;

    public CustomListAdapter(Activity context, String[] itemname) {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.itemname = itemname;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);

        txtTitle.setText(itemname[position]);
        return rowView;
    };
}

public class MainActivity extends Activity {

    private TextView t0;
    private ListView list;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t0 = (TextView) findViewById(R.id.t0);
        list = (ListView)findViewById(R.id.list);

        String[] names = { "Иван", "Марья", "Петр", "Антон", "Даша", "Борис",
                "Костя", "Игорь", "Анна", "Денис", "Андрей" };

        CustomListAdapter adapter = new CustomListAdapter(this, names);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                // String Slecteditem = itemname[position];
                // Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

                update(position);
            }
        });



        context = new Context();
        update(-1);
    }

    public void clickN(View v) {
        update(-1);
    }

    public void update(int num) {
        String text = "";

        context.update(num);

        text += "result: " + context.result + "\n\n";
        text += "text: " + context.text + "\n\n";
        int i = 0;
        List<String> names = context.getTransitionNames();
        for (String name : names) {
            text += "transition: " + i++ + " " + name + "\n";
        }

        t0.setText(text);

        CustomListAdapter adapter = new CustomListAdapter(this, names.toArray(new String[0]));
        list.setAdapter(adapter);
    }
}
